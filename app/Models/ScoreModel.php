<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\Score;

class ScoreModel extends Model {

    protected $table      = 'management_scores';
    protected $primaryKey = 'score_id';

    protected $returnType    = 'App\Entities\Score';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'score_user', 'score_score'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * registra una entrada de score para el usuario indicado
     * @param user el ID del usuario
     * @param score el numero con el que se califica
     * @return true si se puede registrar correctamente el score
     * @return message el mensaje de error si no se puede registra
     */
    public function registra($user, $score) {
        $Score = new Score();
        $Score->user = $user;
        $Score->score = $score;
        try {
            if ( $score >= 0 && $score <= 5 ) {
                if ( $this->save($Score) ) {
                    return true;
                }
            }
            else {
                return "Score cannot be greater than 5 and lower than 0.";
            }
        }
        catch (\mysqli_sql_exception $ex) {
            return 'Database error. Please retry later.';
        }
        catch (\Exception $ex) {
            return 'Exception saving your score.';
        }
        return 'Cannot register your score.';
    }

}