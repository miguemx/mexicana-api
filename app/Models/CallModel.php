<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\Call as Call;

class CallModel extends Model {

    protected $table      = 'core_calls';
    protected $primaryKey = 'call_id';

    protected $returnType    = 'App\Entities\Call';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'call_operator', 'call_car_type', 'call_client', 'call_day_number', 'call_line', 'call_date_time', 'call_timestamp',
        'call_phone', 'call_status', 'call_dispatch_date_time', 'call_dispatch_timestamp', 'call_dispatch_type', 'call_register_type'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function getAll1() {
        
        $query   = $builder->get();
        $rows = $query->getResult();
        return ( $rows );
    }

    /**
     * obtiene todos los examenes de areas (bloques de area)
     * @param filtros (opcional) un arreglo con los filtros a aplicar en la consulta
     * @param regXPagina (opcional) indica el numero de registros que devolvera el arreglo
     * @param pagina (opcional) el numero de pagina que regresara
     */
    public function getAll( $filtros=array(), $regXPagina=25, $pagina=1  ) {
        $this->builder = $this->db->table('view_call_information');
        $registros = $this->createPage( $filtros, $regXPagina, $pagina );
        $registros['pagina'] = $pagina;
        $registros['datos'] = array();
        $this->builder->select('*');
        $this->createFilter($filtros);
        $this->builder->limit( $regXPagina, $registros['inicio'] );

        $query = $this->builder->get();
        $rows = $query->getResult();
        foreach ( $rows as $row ) {
            $registros['datos'][] = $row;
        }

		return $registros;
    }

    /**
     * crea un filtro para la consulta, y selecciona la cantidad de registros y su respectivo indice de inicio y fin
     */
    private function createPage( $filtros, $regXPagina, $pagina) {
        $this->builder->select('COUNT(call_id) as REG');
        $this->createFilter($filtros);
        $query = $this->builder->get();
        $rows = $query->getResult();
        $numRows = intval($rows[0]->REG);

        $paginas = ceil( $numRows/$regXPagina );
        $inicio = ($pagina - 1 ) * $regXPagina;

        if ( $numRows <= $inicio ) {

            $inicio = 0;
        }

        return (
            array(
                'totalRegistros' => $numRows,
                'inicio' => $inicio,
                'regXPagina' => $regXPagina,
                'paginas' => $paginas
            )
            );
    }

    /**
     * crea las clausulas where para la consulta de obtencion de examenes de area
     * @param filtros un array con las llaves como condiciones, y sus valores como la busqueda en la BD
     * @author miguemx
     */
    private function createFilter( $filtros ) {
        if ( !is_null($filtros) ) {
            if ( is_array($filtros) ) {
                if ( array_key_exists('id', $filtros) ) { // filtrar por area
                    if ( $filtros['id'] ) {
                        $this->builder->where( 'call_id', $filtros['id'] );
                    }
                }
            }
        }
        
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * realiza el proceso para crear una llamada y sus respectivas dependencias
     * @param requestData los datos que vienen filtrados desde el controlador
     * @param pickup el objeto del lugar de recogida
     * @param destination el objeto del lugar de destino
     * @return callRegister registro creado de la llamada; null si no se puede crear
     */
    public function registra($requestData, $pickup, $destination) {
        $call = new Call();
        $TripModel = new TripModel();
        $call->client = ($requestData['client']=='0') ? null: $requestData['client'];
        $call->carType = $requestData['carType'];
        $call->status = 'NEW';
        $call->dayNumber = $this->getNext();
        $call->dateTime = date('Y-m-d H:i:s');
        $call->timestamp = time();
        $call->registerType = $requestData['registerType'];
        try {
            if ( $this->save( $call ) ) {
                $call->id = $this->lastId();
                $trip = $TripModel->registra( $call, $pickup, $destination );
                if ( $trip ) {
                    $trip->id = $TripModel->lastId();
                    return ['status'=>'ok', 'message'=>'Trip requested successfully.', 'data'=>['call'=>$call, 'trip'=>$trip] ];
                }
                else {
                    $this->delete( $call->id );
                }
            }
        }
        catch (\Exception $ex) {
            return ['status'=>'error', 'message'=>'Exception registering request or trip information.', 'data'=>$ex->getMessage()];
        }
        return ['status'=>'error', 'message'=>'Cannot register request or trip information.', 'data'=>null];
    }

    /**
     * obtiene el siguiente numero de llamada diaria
     */
    protected function getNext() {
        $this->builder = $this->db->table( $this->table );
        $this->builder->selectMax('call_day_number');
        $this->builder->like('call_date_time', date('Y-m-d'), 'after');
        $this->builder->where('deleted_at IS NULL');
        $query   = $this->builder->get();
        $rows = $query->getResult();
        $max = $rows[0]->call_day_number;
        if ( is_null($max) ) {
            return 1;
        }
        else {
            return $max+1;
        }
    }

}