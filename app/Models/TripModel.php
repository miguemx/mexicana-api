<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Entities\Trip as Trip;

use \App\Libraries\StripePcs as StripePcs;

class TripModel extends Model
{
    protected $table      = 'core_trips';
    protected $primaryKey = 'trip_id';

    protected $returnType    = 'App\Entities\Trip';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'trip_call', 'trip_driver', 'trip_pickup', 'trip_destination', 'trip_status', 'trip_amount',
        'trip_eta', 'trip_distance', 'trip_start_time', 'trip_end_time', 
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * crea un nuevo registro de TRIP basado en la informacion de la llamada (request) y los puntos de destino
     * @param call el objeto Call creado para registrar la llamada
     * @param pickup el objeto con el lugar de encuentro o recogida
     * @param destination el objeto con el lugar de destino
     * @return trip el objeto del viaje creado en la bd
     * @return null si no se puede crear el viaje
     */
    public function registra($call, $pickup, $destination) {
        $trip = new Trip();
        $trip->call = $call->id;
        $trip->pickup = $pickup->id;
        $trip->destination = $destination->id;
        $trip->status = 'REQUESTED';
        // -- patch temporal para obtener el numero de pasajeros
        $passNumber = 4;
        switch($call->carType) {
            case 2: $passNumber=5; break;
            case 3: $passNumber=6; break;
            case 4: $passNumber=8; break;
            default: $passNumber=4; break;
        }
        // -- fin del patch temporal
        
        $distanceAndTime = $this->getDistanceAndTime( $pickup->mapsCoord, $destination->mapsCoord );
        $trip->eta = $distanceAndTime['timeText'] ; // el tiempo se guarda en minutos
        $trip->distance = $distanceAndTime['distance'] / 1000; // la distancia se guarda en kilometros
        $trip->amount = $this->calculaAmount( $trip->distance, $trip->eta, $distanceAndTime['toll'], $pickup->mapsCoord, $destination->mapsCoord, $passNumber );

        if ( $this->save($trip) ) {
            $trip->id = $this->lastId();
            return $trip;
        }

        return null;
    }

    /**
     * genera un estimado de viaje en cuanto a distancia y tiempo
     */
    public function estima($pickupCoord, $destinationCoord) {
        $dAndT = $this->getDistanceAndTime( $pickupCoord, $destinationCoord ); // obtiene distance and time

        $this->builder = $this->db->table('core_car_types')->orderBy('cartype_pass_number ASC')->get();
        $carTypes = $this->builder->getResult();

        foreach( $carTypes as $carType ) {
            $estimate[  ] = array(
                'id' => $carType->cartype_id,
                'name' => $carType->cartype_name,
                'distance' => $dAndT['distance'],
                'time' => $dAndT['time'],
                'distanceText' => $dAndT['distanceText'],
                'timeText' => $dAndT['timeText'],
                'hasTolls' => $dAndT['toll'],
                'passanger' => $carType->cartype_pass_number,
                'amount' => $this->calculaAmount( $dAndT['distance']/1000, $dAndT['time'], $dAndT['toll'], $pickupCoord, $destinationCoord, $carType->cartype_pass_number ),
            );
        }
        return $estimate;
    }

    /**
     * obtiene un arreglo con la distancia y tiempo de recorrido, basado en la API de google maps matrix
     * @param pickupCoord las cordenadas de recogida
     * @param destinationCoord las coordenadas de destino
     * @return distanceAndTime un arreglo con la disntacia en metros, el tiempo en segundos y lo textos descriptivos
     */
    protected function getDistanceAndTime($pickupCoord, $destinationCoord) {
        $distanceAndTime = array( 'distance' => '0', 'time'=>'0', 'distanceText'=>'0mi', 'timeText'=>'0 min', 'toll'=>false );
        $url = MAPS_DIRECTIONS.'key='.env('maps.apikey').'&origin='.$pickupCoord.'&destination='.$destinationCoord.'&language=en';
        
        $ch = curl_init(); // create curl resource
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return the transfer as a string
        $output = curl_exec($ch); // $output contains the output string
        curl_close($ch);  // close curl resource to free up system resources

        $data = json_decode($output, true);
        if ( $data['status'] === 'OK' ) {
            if ( count( $data['routes'] ) > 0 ) { // en caso de existir una ruta al menos, verificar los datos
                if ( count($data['routes'][0]['legs']) > 0 ) {
                    $route = $data['routes'][0]['legs'][0];
                    $distanceAndTime = array(
                        'distance' => $route['distance']['value'],
                        'time' => $route['duration']['value'],
                        'distanceText' => $route['distance']['text'],
                        'timeText' => $route['duration']['text'],
                        'toll' => $this->findTolls( $route['steps'] ),
                    );
                }
            }
        }
        
        return $distanceAndTime;
    }

    /**
     * calcula el monto a cobrar por el viaje, basado en la distancia y el tiempo de recorrido
     * @param distance la distancia en kilometros, ojo, KILOMETROS
     * @param time el tiempo en segundos
     * @param toll true si la ruta encontrata tiene casetas; false en caso de no tenerlas
     * @param pickupCoord las coordenadas de recogida en caso de ser necesarias
     * @param destinationCoord las coordenadas de destino en caso de ser necesarias
     * @param passNumber (opcional) el numero de pasajeros
     * @return amount el monto en dolares a cobrar
     */
    protected function calculaAmount($distance, $time, $toll, $pickupCoord, $destinationCoord, $passNumber=4) {
        $mi = $this->m2mi( $distance ); // convertir los kilometros a millas
        $amount = $mi * 1.1 + 5; // calcular el monto por milla a 1.1 dolares + 5 dolares de banderazo

        if ( $toll ) { // si se detectan casetas, se debera cobrar
            $amount += $this->getTollCost( $pickupCoord, $destinationCoord ); // obtener el costo de casetas
        }
        
        $amount = 1.25 * $amount; // comision del 25% por anadidura de impuestos, permisos y ganancia
        if ( $passNumber > 4 ) { // se toma como base 4 pasajeros (menos se cobra lo mismo, mas se incrementa el costo)
            $amount = $passNumber * $amount / 4;
        }
        $comision = $amount * 0.029 + 0.31; // calcular la comision de stripe
        $amount = $amount + $comision; // sumar al monto calculado,la comision de stripe
        $amount = floatval ( number_format($amount, 2, '.', '') ); // redondear a dos decimales
        
        return $amount;
    }

    /**
     * convierte metros a millas
     * @param meters el numero de metros
     * @return millas el numero en millas
     */
    protected function m2mi($meters) {
        $millas = $meters * 0.62137;
        return $millas;
    }

    /**
     * busca si la ruta seleccionada tiene tolls en el camino
     * @param pasos el objeto que contiene los pasos a seguir en busqueda de tolls o rutas de cuota
     * @return tolls indica true si encontro un toll o false en caso de no haber
     */
    protected function findTolls( $pasos ) {
        $toll = false;
        foreach( $pasos as $paso ) {
            if ( stripos( $paso['html_instructions'], 'toll' ) !== false ) {
                $toll = true;
                break;
            }
        }
        return $toll;
    }

    /**
     * busca el costo de las casetas desde las coordenadas determinadas
     * @param pickup las coordenadas de recogida
     * @param destination las coordenadas de destinatario
     * @return cost el costo de las casetas
     */
    protected function getTollCost( $pickup, $destination ) {
        $cost = 0;
        $url = TOLL_ROUTES;
        $pickupDataCoord = explode( ',', $pickup );
        $destDataCoord = explode( ',', $destination );

        $params = array(
            "from" => array( "lat" => $pickupDataCoord[0], "lng" => $pickupDataCoord[1]),
            "to"   => array( "lat" => $destDataCoord[0],   "lng" => $destDataCoord[1]),
            "fuelPriceCurrency" => "USD" 
        );
        
        $ch = curl_init(); // create curl resource
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json', 'x-api-key: '.env('toll.apikey')) );
        curl_setopt($ch, CURLOPT_POST,           1 );
        curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode($params) ); 
        curl_setopt($ch, CURLOPT_URL,            $url); // set url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return the transfer as a string
        $output = curl_exec($ch); // $output contains the output string
        curl_close($ch);  // close curl resource to free up system resources
        $data = json_decode($output, true);
        if ( !is_null($data) ) {
            if( $data['status'] == 'OK' ) {
                if( count($data['routes']) > 0 ) {
                    if( !is_null( $data['routes'][0]['costs']['tag'] ) ) {
                        if ( $data['routes'][0]['costs']['tag'] > $cost ) {
                            $cost = $data['routes'][0]['costs']['tag'];
                        }
                    }
                    if( !is_null( $data['routes'][0]['costs']['cash'] ) ) {
                        if ( $data['routes'][0]['costs']['cash'] > $cost ) {
                            $cost = $data['routes'][0]['costs']['cash'];
                        }
                    }
                    if( !is_null( $data['routes'][0]['costs']['prepaidCard'] ) ) {
                        if ( $data['routes'][0]['costs']['prepaidCard'] > $cost ) {
                            $cost = $data['routes'][0]['costs']['prepaidCard'];
                        }
                    }
                }
            }
        }
        $cost = $cost * 2;
        return $cost;
    }

    /**
     * Procesa el pago de un viaje para determinar si se puede ofrecer o no
     * @param trip el objeto Trip con los datos del viaje
     * @param call el objeto CALL que detona el trip
     * @return result arreglo asociativo con el resultado de la operacion y la informacion necesaria
     */
    public function paga( $trip, $call ) {
        $result = array( 'status' => 'error', 'message' => 'Cannot pay your trip.', 'data' => null );
        // obtener los datos del cliente
        $UserModel = new UserModel();
        $CardModel = new CardModel();
        $Stripe = new StripePcs();

        try {
            $user = $UserModel->find( $call->client ); 
            $card = $CardModel->where('card_user', $call->client)->orderBy('card_default DESC')->first();
            $cardData = array(
                'cardNumber' => $card->cardNumber, 
                'expMonth' => $card->expMonth, 
                'expYear' => $card->expYear, 
                'cvc' => $card->cvc
            );
            $paymentData = array( 'monto' => ceil($trip->amount * 100), 
                'moneda' => 'usd', 
                'descripcion' => 'PrivateCarService Trip Service '.$trip->id , 
                'email' => $user->username
            );

            $result = $Stripe->paga( $cardData, $paymentData );
            
        }
        catch ( \Exception $ex) {
            $result = array( 
                'status' => 'error', 
                'message' => 'Could not proccess payment. Please check that client exists and has registred valid credit cards.', 
                'data' => $ex->getMessage() );
        }

        return $result;
    }

    /**
     * takes a trip from a driver 
     * @param trip el objeto del trip a tomar por parte del chofer
     * @param driver el objeto driver
     * @return result array ( status=>ok|error, message=>el mensaje que resulte, data=> los datos del viaje )
     */
    public function take( $trip, $driver ) {
        $response = [ 'status'=>'error', 'message'=>'Operation not completed.', 'data'=>$trip ];
        if ( $driver->role == '3' ) {
            if ( $driver->ontrip == '0' ) {
                if ( $trip->status == 'REQUESTED' ) {
                    $trip->driver = $driver->id;
                    $trip->status = 'DISPATCHED';
                    if ( $this->save( $trip ) ) {
                        $response = [ 'status'=>'ok', 'message'=>'Trip is now ongoing', 'data'=>$trip ];
                    }
                    else {
                        $response = [ 'status'=>'error', 'message'=>'Error updating trip status. Please verify your current status.', 'data'=>$trip ];
                    }
                }
                else {
                    $response = [ 'status'=>'error', 'message'=>'Current trip is already taken by other driver.', 'data'=>null ];
                }
            }
            else {
                $response = [ 'status'=>'error', 'message'=>'Driver is currently in other trip.', 'data'=>null ];    
            }
        }
        else {
            $response = [ 'status'=>'error', 'message'=>'User is not a driver.', 'data'=>null ];
        }
        return $response;
    }

    
}