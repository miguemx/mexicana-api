<?php
namespace App\Models;

use CodeIgniter\Model;

class CardModel extends Model {

    protected $table      = 'customer_cards';
    protected $primaryKey = 'card_id';

    protected $returnType    = 'App\Entities\Card';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'card_id', 'card_user', 'card_number', 'card_exp_month', 'card_exp_year', 'card_cvc', 'card_name', 'card_default'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * obtiene todas las tarjetas registradas de un usuario determinado
     * @param userId el ID del usuario
     * @return cards un arraglo con los datos de todas las tarjetas, o vacio si no existe alguna
     */
    public function getFromUser( $userId ) {
        $cards = $this->where('card_user', $userId)->orderBy('card_default DESC')->findAll();
        $i = 0;
        foreach ( $cards as $card ) {
            $numAsteriscos = strlen($card->cardNumber) - 4;
            $asteriscos = '';
            for ( $j=0; $j<$numAsteriscos; $j++ ) {
                $asteriscos .= '*';
            }
            $cards[$i]->cardNumber = substr( $card->cardNumber, 0, 1 ).$asteriscos.substr( $card->cardNumber, -3 );
            $cards[$i]->cvc = '**'.substr( $card->cvc, -1 );
            $i++;
        }
        return $cards;
    }

    /**
     * buscar una tarjeta por su numero
     * @param cardNumber el numero de la tarjeta
     * @return card el objeto de tarjeta, o null si no se encuentra
     */
    public function findByNumber( $cardNumber ) {
        $card = $this->where('card_number', $cardNumber)->first();
        return $card;
    }

    /**
     * marca como uso por defecto una tarjeta de un cliente determinado
     * @param idUser el ID del cliente
     * @param idCard el ID de la tarjeta
     * @return result true si se puede hacer default; false si no se puede hacer default
     */
    public function makeDefaultForCustomer( $idUser, $idCard ) {
        // quitar cualquiera que pudiese estar como default
        $result = $this->where('card_user', $idUser)->set(['card_default' => '0'])->update();
        // poner la seleccionada en default
        $result = $this->where('card_user', $idUser)->where('card_id', $idCard)->set(['card_default' => '1'])->update();
        return $result;
    }

    /**
     * elimina una tarjeta de un cliente determinado; primero se valida que la tarjeta le corresponde al cliente
     * @param idUser el ID del usuario
     * @param idCard el ID de la tarjeta a eliminar
     * @return result true si se logra eliminar, false si ocurre algun problema
     */
    public function remove( $iduser, $idCard ) {
        $result = false;
        $query = $this->where('card_user',$iduser)->where('card_id',$idCard)->findAll();
        if( count($query) > 0 ) {
            $result = $this->delete( $idCard );
        }
        return $result;
    }

}