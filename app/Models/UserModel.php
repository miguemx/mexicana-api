<?php

namespace App\Models;
use CodeIgniter\Model;

class UserModel extends Model {
    
    protected $table      = 'access_users';
    protected $primaryKey = 'user_id';

    protected $returnType    = 'App\Entities\User';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'user_role', 'user_person', 'user_username', 'user_password', 'user_client', 
        'user_status', 'user_online', 'user_ontrip', 'user_validation_token'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    protected $configCrypt;
    protected $encrypter;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();

        $this->configCrypt    = new \Config\Encryption();      // load the configuration for the encryption service
        $this->encrypter = \Config\Services::encrypter($this->configCrypt); // start the encryption service
    }

    /**
     * searchs a specifc user by their username
     * @param username the username to search
     * @return users user's data found or null if not exists that username
     */
    public function searchUserName($username) {
        $user = $this->where('user_username', $username)->first();
        return $user;
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * valida los datos en la base de datos para hacer login o inicio de sesion
     * @param username el nombre de usuario a buscar
     * @param password la contraseña para validar que es el usuario
     * @return status el resultado si se puede o no iniciar sesion
     */
    public function login($username, $password) {
        $user = $this->searchUserName($username);
        if ( !is_null($user) ) {
            $password = sha1( $password );
            if ( $password === $user->password ) {
                $user->password = '';
                return $user;
            }
        }
        
        return null;
    }

    /**
     * crea un nuevo registro de un usuario
     * @param userEntity la entidad del usuario a crear
     */
    public function registra( $userEntity ) {
        $userEntity->password = sha1( $userEntity->password );
        $userEntity->status = 'ACTIVE';
        $userEntity->validationToken = $this->creaToken( $userEntity->username );
        if ( $this->save( $userEntity ) ) { // crear el registro del usuario
            $userEntity->id = $this->lastID();
            return $userEntity;
        }
        else {
            return null;
        }
    }

    /**
     * obtiene el perfil del usuario y genera la informacion en un JSON
     * @return result los datos de usuario y persona, o null si el usuario no existe
     */
    public function getPerfil( $idUser ) {
        $result = null;
        $user = $this->find($idUser);
        if ( $user ) {
            $personModel = new PersonModel();
            $persona = $personModel->find( $user->person );
            if ( $persona ) {
                $user->password = '';
                $result = array(
                    'user' => $user,
                    'persona' => $persona
                );
            }
        }
        return $result;
    }

    /**
     * genera un token cifrado para registrar a un usuario y permitir su posterior validacion
     * @param username el nombre de usuario del usaurio (generalmente sera el email)
     * @return token el token cifrado en base 64
     */
    public function creaToken( $username ) {
        $timestamp = time();
        $token = $username.','.$timestamp;
        $cipher = $this->encrypter->encrypt( $token );
        $token = base64_encode( $cipher );
        $token = str_replace( '/', '-DiaG-', $token );
        $token = str_replace( '+', '-MaS-', $token );
        return $token;
    }

    /**
     * valida un token recibido; 
     * @return true en caso de que el token sea valido
     * @return message string con el mensaje de error en caso de que el token no sea reconocido
     */
    public function validaToken($token) {
        $result = "Validation proccess could not start corectly.";
        $token = urldecode( $token );
        $token = str_replace( '-DiaG-', '/', $token );
        $token = str_replace( '-MaS-', '+', $token );
        try {
            $dataToken = base64_decode( $token );
            $dataToken = $this->encrypter->decrypt( $dataToken );
            $data = explode( ',', $dataToken );
            $user = $this->where('user_username', $data[0])->first();
            if ( !is_null($user) ) {
                $timestamp = time();
                $dif = floor( ($timestamp - $data[1]) / 60 );
                if ( $dif <= 60 ) {
                    $user->validationToken = null;
                    $user->active = '1';
                    $this->save( $user );
                    $result = true;
                }
                else {
                    $result = "Validation token has expired.";    
                }
            }
            else {
                $result = "User not found. ";
            }
        }
        catch (\CodeIgniter\Format\Exceptions\FormatException $ex) {
            $result = "Error validating token. ".$ex->getMessage();
        }
        catch ( \Exception $ex ) {
            $result = "Error validating token. ".$ex->getMessage();
        }
        return $result;
    }

    /**
     * pone el trip status de un usuario (cliente o chofer) para saber si esta en viaje o no
     * @param user el objeto de usuario, o una cadena con su ID
     * @param status true para indicar que esta en viaje, false para indicar que ya no esta en viaje
     * @return true si se puede poner
     * @return false si ocurre un error
     */
    public function updateTripStatus($user, $status) {
        $tipo = gettype( $user );
        var_dump($tipo);
        $user->ontirp = ( $status )? '1': '0';
        return $this->save( $user );
    }

}