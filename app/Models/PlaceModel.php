<?php
namespace App\Models;
use CodeIgniter\Model;
use App\Entities\Place as Place;

class PlaceModel extends Model {
    protected $table      = 'core_places';
    protected $primaryKey = 'place_id';

    protected $returnType    = 'App\Entities\Place';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'place_maps_id', 'place_maps_coord', 'place_maps_name', 'place_number', 'place_street',
        'place_city', 'place_state', 'place_zip', 'place_country'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted in database
     * @return insertID the las ID inserted in database
     */
    public function lastId() {
        return $this->db->insertID();
    }

    /**
     * registra un lugar con base en la informacion obtenida. 
     * Intentara primero buscar si el lugar solicitado para registro, existe
     * @param data la informacion del lugar
     * @return App\Entities\Place el lugar creado, o el ya existente;
     */
    public function registra($data) {
        if ( !array_key_exists('mapsId', $data) ) {
            $data['mapsId'] = null;
        }
        if ( !$data['mapsId'] ) {
            $data = $this->getPlaceByLocation( $data['mapsCoord'] );
        }
        $place = $this->findByMapsId($data['mapsId']);
        if ( is_null( $place ) ) {
            $place = new Place( $data );
            if ( $this->save( $place ) ) {
                $place->id = $this->lastID();
            }
        }
        return $place;
    }

    /**
     * busca un lugar mediante su ID de google maps
     */
    public function findByMapsId($mapsId) {
        $place = $this->where('place_maps_id', $mapsId)->first();
        return $place;
    }

    /**
     * busca en la api de geocoding de google, los datos del lugar con base en sus coordenadas
     * @param coordenadas las coordenadas en formato lat,lng (por ejemplo: "40.9309257,-73.895721")
     * @return place el lugar obtenido por la api
     */
    protected function getPlaceByLocation($coordenadas) {
        $place = null;
        $url = MAPS_GEOCODE_URL.'key='.env('maps.apikey').'&latlng='.$coordenadas.'&language=en';
        
        $ch = curl_init(); // create curl resource
        curl_setopt($ch, CURLOPT_URL, $url); // set url
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //return the transfer as a string
        $output = curl_exec($ch); // $output contains the output string
        curl_close($ch);  // close curl resource to free up system resources

        $data = json_decode($output, true);
        if ( $data['status'] === 'OK' ) {
            if( count($data['results']) ) {
                $place = array(
                    'mapsId' => $data['results'][0]['place_id'],
                    'mapsCoord' => $coordenadas,
                    'mapsName' => $data['results'][0]['formatted_address'],
                    'number' => $this->searchFieldAddress($data['results'][0]['address_components'],'street_number'),
                    'street' => $this->searchFieldAddress($data['results'][0]['address_components'],'route'),
                    'city' => $this->searchFieldAddress($data['results'][0]['address_components'],'locality'),
                    'state' => $this->searchFieldAddress($data['results'][0]['address_components'],'administrative_area_level_1'),
                    'zip' => $this->searchFieldAddress($data['results'][0]['address_components'],'postal_code'),
                    'country' => $this->searchFieldAddress($data['results'][0]['address_components'],'country'),
                );
            }
        }
        return $place;
    }

    /**
     * busca un campo determinado en los resultados de la API de google maps para obtener un dato especifico de una direccion
     * @param addressComponents los componentes de direccion como los regresa la API de gooogle maps
     * @param field el nombde del campo a buscar
     * @return name el nombre del valor buscado
     */
    protected function searchFieldAddress($addressComponents, $field) {
        $name = '';
        for ( $i=0; $i<count($addressComponents); $i++ ) {
            for ( $j=0; $j<count($addressComponents[$i]['types']); $j++ ) {
                if ( $addressComponents[$i]['types'][$j] == $field ) {
                    $name = $addressComponents[$i]['long_name'];
                    break;
                }
            }
        }
        return $name;
    }

}