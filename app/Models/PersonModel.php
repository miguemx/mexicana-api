<?php

namespace App\Models;
use CodeIgniter\Model;

class PersonModel extends Model {
    
    protected $table      = 'management_persons';
    protected $primaryKey = 'person_id';

    protected $returnType    = 'App\Entities\Person';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'person_name', 'person_address', 'person_phone', 'person_cellphone', 'person_email',
        'person_city', 'person_state', 'person_zip', 'person_birth', 'person_ss_num', 'person_us_citizen'
    ];

    protected $useTimestamps = true;

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    protected $db;
    protected $builder;

    public function __construct() {
        parent::__construct();

        $this->db = \Config\Database::connect();
        $this->db = db_connect();
    }

    /**
     * returns last id inserted
     * @return lastID the last ID inserted
     */
    public function lastId() {
        return $this->db->insertID();
    }

}