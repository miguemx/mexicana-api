<?php

namespace App\Controllers;

use App\Models\UserModel as UserModel;
use App\Models\PersonModel as PersonModel;
use App\Entities\User as User;
use App\Entities\Person as Person;

class Login extends BaseController {

    protected $users;

    public function __construct() {
        $this->users = new UserModel();
    }

    public function index() {
        $this->response->setHeader('Content-Type', 'application/json');
        $this->response->setStatusCode(401);
        $response = array( 'status' => 'error','message' => 'Unauthorized','data' => '401' );
        echo json_encode( $response );
    }

    /**
     * Procesa el inicio de sesion de los usuarios
     */
    public function login() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $validation =  \Config\Services::validation();
        $this->response->setHeader('Content-Type', 'application/json');
        $data = $this->request->getJSON(true); 
        if ( $validation->run( $data, 'login' ) ) { // validar la integridad de los campos
            $user = $this->users->login( $data['username'], $data['password'] );
            if ( !is_null($user) ) {
                if( $user->status == 'ACTIVE' ) {
                    $response = array( 'status' => 'ok', 'message' => 'Login successful.', 'data' => $user );
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Username is not active. Please validate your email or reset your password.', 'data' => null );        
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Username and/or password do not match.', 'data' => null );    
            }
        }
        else {
            $errors = array();
            foreach ( $validation->getErrors() as $error ) {
                $errors[] = $error;
            }
            $response = array( 'status' => 'error', 'message' => 'You cannot login. Please check error messages.', 'data' => $errors );
        }
        echo $this->cleanResponse( $response );
    }

}