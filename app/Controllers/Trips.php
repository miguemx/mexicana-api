<?php

namespace App\Controllers;

use App\Models\UserModel as UserModel;
use App\Models\TripModel as TripModel;
use App\Models\CallModel as CallModel;
use App\Models\PlaceModel as PlaceModel;

class Trips extends BaseController
{
	protected $UserModel;
	protected $TripModel;
	protected $CallModel;
	protected $PlaceModel;

	public function __construct() {
		$this->UserModel = new UserModel();
		$this->TripModel = new TripModel();
		$this->CallModel = new CallModel();
		$this->PlaceModel = new PlaceModel();
	}

	public function index()
	{
		$response = array( 'status' => 'error', 'message' => '401. Unauthorized.', 'data' => null );
		$this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse( $response );
	}

	/**
	 * ejecuta las acciones para que un chofer tome un viaje (actualizar estados y tiempos)
	 */
	public function take() {
		$response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
		$data = $this->request->getJSON(true); 
		if ( !is_null($data) ) {
			if ( array_key_exists('driver',$data) && array_key_exists('trip', $data) ) {
				$driver = $this->UserModel->find($data['driver']);
				$trip = $this->TripModel->find($data['trip']);
        		if ( !is_null($driver) && !is_null($trip) ) {
					$call = $this->CallModel->find( $trip->call );
					if ( !is_null($call) ) {
						$response = $this->TripModel->take( $trip, $driver );
						if ( $response['status'] == 'ok' ) {
							$driver->ontrip = '1';
							$call->status = 'DISPATCHED';
							$call->dispatchDateTime = date("Y-m-d H:i:s");
							$call->dispatchTimestamp = time();
							$call->dispatchType = 'DRIVER';
							try {
								$this->UserModel->save( $driver );
								$this->CallModel->save( $call );
							}
							catch(\Exception $ex) {
								$response = [ 'status'=>'error', 'message'=>'Possible duplicate of information.', 'data'=>null ];			
							}
						}
					}
					else {
						$response = [ 'status'=>'error', 'message'=>'Cannot locate client\'s request.', 'data'=>null ];	
					}
				}
				else {
					$response = [ 'status'=>'error', 'message'=>'Trip information is incomplete.', 'data'=>null ];
				}
			}
			else {
				$response = [ 'status'=>'error', 'message'=>'Cannot find information for driver or trip.', 'data'=>null ];	
			}
		}
		else {
			$response = [ 'status'=>'error', 'message'=>'Incorrect parameters.', 'data'=>null ];
		}
		$this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse( $response );
	}

	/**
	 * accion que se ejecuta cuando el chofer 
	 */
	public function inicia() {
		$response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
		$data = $this->request->getJSON(true); 
		if ( !is_null($data) ) {
			if ( array_key_exists('driver',$data) && array_key_exists('trip', $data) ) {
				$trip = $this->TripModel->find( $data['trip'] );
				if ( $trip ) {
					if ( $trip->driver == $data['driver'] && $trip->status=='DISPATCHED' ) {
						$trip->status = 'ONGOING';
						$trip->startTime = time();
						if ( $this->TripModel->save( $trip ) ) {
							$trip->client = 0;
							$call = $this->CallModel->find( $trip->call );
							if ( $call->client ) {
								$client = $this->UserModel->find( $call->client ) ;
								if ( $client ) {
									$trip->client = $client->id;
								}
							}
							$response = [ 'status'=>'ok', 'message'=>'Trip started successfully.', 'data'=>$trip ];		
						}
						else {
							$response = [ 'status'=>'error', 'message'=>'Cannot save trip status.', 'data'=>null ];	
						}
					}
					else {
						$response = [ 'status'=>'error', 'message'=>'Incorrect trip information; status or driver invalid.', 'data'=>null ];		
					}
				}
				else {
					$response = [ 'status'=>'error', 'message'=>'Trip information not found.', 'data'=>null ];		
				}
			}
			else {
				$response = [ 'status'=>'error', 'message'=>'Incomplete parameters.', 'data'=>null ];	
			}
		}
		else {
			$response = [ 'status'=>'error', 'message'=>'Incorrect parameters.', 'data'=>null ];
		}
		$this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse( $response );
	}

	/**
	 * ejecuta las acciones para dar por terminado un viaje
	 */
	public function finaliza() {
		$response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
		$data = $this->request->getJSON(true); 
		if ( !is_null($data) ) {
			if ( array_key_exists('driver',$data) && array_key_exists('trip', $data) ) {
				$trip = $this->TripModel->find( $data['trip'] );
				if ( $trip ) {
					if ( $trip->driver == $data['driver'] && $trip->status=='ONGOING' ) {
						$trip->status = 'COMPLETED';
						$trip->endTime = time();
						if ( $this->TripModel->save( $trip ) ) {
							$trip->client = 0;
							$call = $this->CallModel->find( $trip->call );
							if ( $call->client ) {
								$client = $this->UserModel->find( $call->client ) ;
								if ( $client ) {
									$trip->client = $client->id;
									$client->ontrip = '0';
									$this->UserModel->save( $client );
								}
								$driver = $this->UserModel->find( $trip->driver );
								if ( $driver ) {
									$driver->ontrip = '0';
									$this->UserModel->save( $driver );
								}
							}
							$response = [ 'status'=>'ok', 'message'=>'Trip completed successfully.', 'data'=>$trip ];		
						}
						else {
							$response = [ 'status'=>'error', 'message'=>'Cannot save trip status.', 'data'=>null ];	
						}
					}
					else {
						$response = [ 'status'=>'error', 'message'=>'Incorrect trip information; status or driver invalid.', 'data'=>null ];		
					}
				}
				else {
					$response = [ 'status'=>'error', 'message'=>'Trip information not found.', 'data'=>null ];		
				}
			}
			else {
				$response = [ 'status'=>'error', 'message'=>'Incomplete parameters.', 'data'=>null ];	
			}
		}
		else {
			$response = [ 'status'=>'error', 'message'=>'Incorrect parameters.', 'data'=>null ];
		}
		$this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse( $response );
	}

	/**
	 * busca la informacion del viaje
	 * @param id el ID del viaje a buscar (se debe buscar por ID de viaje, las llamadas se buscan en el controlador Calls)
	 */
	public function ver($id) {
		$response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
		if ( is_numeric($id) ) {
			$trip = $this->TripModel->find( $id );
			if ( $trip ) {
				$call = $this->CallModel->find( $trip->call );
				$pickup = $this->PlaceModel->find( $trip->pickup );
				$destination = null;
				if ( $trip->destination ) {
					$destination = $this->PlaceModel->find( $trip->destination );
				}
				if ( $call ) {
					$data = [ 'trip'=>$trip, 'call'=>$call, 'pickup'=>$pickup, 'destination'=>$destination ];
					if ( $call->client ) {
						$client = $this->UserModel->find( $call->client );
						if ( $client ) {
							$client->password = null;
							$client->validationToken = null;
							$data['client'] = $client;
						}
					}
					$response = [ 'status'=>'ok', 'message'=>'Trip information listed correctly.', 'data'=> $data ];
				}
				else {
					$response = [ 'status'=>'error', 'message'=>'Request for trip not found.', 'data'=>null ];
				}
			}
			else {
				$response = [ 'status'=>'error', 'message'=>'Trip not found.', 'data'=>null ];	
			}
		}
		else {
			$response = [ 'status'=>'error', 'message'=>'Incorrect parameters.', 'data'=>null ];
		}
		$this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse( $response );
	}

}