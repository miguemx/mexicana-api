<?php
namespace App\Controllers;

use \App\Libraries\StripePcs as StripePcs;
use \App\Models\CardModel as CardModel;
use \App\Models\UserModel as UserModel;
use \App\Entities\Card as Card;

class PaymentMethods extends BaseController {

    protected $validation;
    protected $Stripe;

    protected $CardModel;
    protected $UserModel;

    public function __construct() {
        $this->Stripe = new StripePcs();
        $this->validation = \Config\Services::validation();

        $this->CardModel = new CardModel();
        $this->UserModel = new UserModel();
    }

    public function index() {
        echo 'paymentMethod';
    }

    /**
     * agrega un registro de tarjeta de credito de cliente, simpre y cuando stripe aprueba el pago de $0.50 USD
     * muestra la salida del proceso
     */
    public function addCard() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( $this->validation->run( $data, 'card' ) ) { // validar la integridad de los campos
            $data['cardNumber'] = preg_replace( "/\s/", '', $data['cardNumber'] );
            $user = $this->UserModel->find( $data['userId'] );
            if ( !is_null($user) ) {
                if ( is_null( $this->CardModel->findByNumber($data['cardNumber']) ) ) {
                    $responseStripe = $this->Stripe->addCard( $data );
                    if( $responseStripe['status'] == 'ok' ) {
                        $data['user'] = $user->id;
                        $data['name'] = strtoupper( $data['name'] );
                        $card = new Card( $data );
                        if ( $this->CardModel->save( $card ) ) {
                            $response = array( 'status' => 'ok', 'message' => 'Card added successfully.', 'data' => $responseStripe );
                        }
                        else {
                            $response = array( 'status' => 'error', 'message' => 'Could not add your card.', 'data' => null );
                        }
                    }
                    else {
                        $response = $responseStripe;
                    }
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Card already exists.', 'data' => null );
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Customer not found.', 'data' => null );
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Please verify the following errors.', 'data' => $this->getValidationErrors($this->validation) );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $response );
    }

    /**
     * pone la bandera de default a una tarjeta de un usuario determinado
     * los datos se reciben por post
     */
    public function makeCardDefault() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( is_numeric($data['userId']) && is_numeric($data['cardId']) ) { // validar la integridad de los campos
            $user = $this->UserModel->find( $data['userId'] );
            if ( !is_null($user) ) {
                if ( $this->CardModel->makeDefaultForCustomer( $user->id, $data['cardId'] ) ) {
                    $response = array( 'status' => 'ok', 'message' => 'New default card.', 'data' => null );    
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Cannot make this card your default card.', 'data' => null );    
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Customer does not exist.', 'data' => null );    
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Incorrect input.', 'data' => null );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $response );
    }

    /**
     * elimina una tarjeta de un usuario
     */
    public function deleteCard() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( is_numeric($data['userId']) && is_numeric($data['cardId']) ) { // validar la integridad de los campos
            $user = $this->UserModel->find( $data['userId'] );
            if ( !is_null($user) ) {
                if ( $this->CardModel->remove( $user->id, $data['cardId'] ) ) {
                    $response = array( 'status' => 'ok', 'message' => 'Card removed successfully.', 'data' => null );    
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Cannot remove this card.', 'data' => null );    
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Customer does not exist.', 'data' => null );    
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Incorrect input.', 'data' => null );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $response );
    }

}