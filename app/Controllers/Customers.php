<?php
namespace App\Controllers;

use \App\Models\CardModel as CardModel;
use \App\Models\UserModel as UserModel;

class Customers extends BaseController {

    protected $validation;
    protected $UserModel;
    protected $CardModel;

    public function __construct() {
        $this->validation =  \Config\Services::validation();
        $this->UserModel = new UserModel();
        $this->CardModel = new CardModel();
    }

    public function index() {
        $this->response->setHeader('Content-Type', 'application/json');
        echo 'null';
    }

    /**
     * entrega la informacion de las tarjetas que dio de alta un cliente
     * @param id el ID del usuario
     */
    public function cards($id) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
        $data = [ 'id'=>$id ];
        if ( $this->validation->run( $data, 'urlparameters') ) {
            $user = $this->UserModel->find( $id );
            if ( !is_null($user) ) {
                $cards = $this->CardModel->getFromUser( $user->id );
                $response = array( 
                    'status' => 'ok', 'message' => 'Customer cards listed correctly.', 'data' => array( 'user' => $user->id, 'cards' => $cards )  
                );
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'User not found.', 'data' => null );
            }
            
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Validation errors.', 'data' => $this->getValidationErrors($this->validation) );
        }

        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}