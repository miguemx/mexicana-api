<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
	}

	/**
	 * pasa al fomtato json la respuesta para eliminar los campos inecesarios
	 * @param data el arreglo final con el cual se hara la respuesta
	 * @return response la respuesta en formato json para ser renderizada por el controlador
	 */
	public function cleanResponse($data) {
		$response = json_encode ( $data ); // convertir en string la respuesta para evitar trabajar con objetos
		$response = json_decode ( $response, true ); // regresar a array asociativo la respuesta
		$response = $this->cleanData( $response ); // eliminar las llaves innecesarias y se sigue manteniendo el array
		$response = json_encode( $response ); // volver a generar el string json para ser renderizado
		return $response;
	}

	/**
	 * elimina los campos inecesarios de la data que sera enviada como respuesta
	 * @param data los datos en un array asociativo 100%
	 * @return data los datos ya limpios
	 */
	private function cleanData( $data ) {
		foreach ( $data as $key=>$value ) {
			if ( gettype($data[$key]) == 'array' ) {
				$data[$key] = $this->cleanData( $data[$key] );
			}
			if ( strpos( $key, '_') !== false ) {
				unset( $data[$key] );
			}
		}
		return $data;
	}

	/**
     * obtiene los datos de un arreglo que tenga las llaves con un prefijo para identificar su tipo de informacion
     * @param data el arreglo donde vienen los datos que se desea obtener
     * @param tipo una cadena con el prefijo
     * @return prefixedData el arreglo con los datos obtenidos; las llaves regresan de forma camelCase
     */
    protected function getPrefixedData($data, $tipo) {
        $prefixedData = array();
        foreach ( $data as $key=>$value ) {
            if ( preg_match("/^".$tipo."/", $key) ) {
                $key = str_replace( $tipo, '', $key );
                $key = lcfirst( $key );
                $prefixedData[$key] = $value;
            }
        }
        
        return ( $prefixedData );
	}
	
	/**
     * regresa los errores detectados por la libreria Validation en un arreglo con los textos
	 * @param validation le instancia de la libreria validation
	 * @return errors un arreglo con los mensajes de error
     */
    protected function getValidationErrors($validation) {
        $errors = array();
        foreach ( $validation->getErrors() as $error ) {
            $errors[] = $error;
        }
        return $errors;
	}

}
