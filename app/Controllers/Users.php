<?php

namespace App\Controllers;

use App\Models\UserModel as UserModel;
use App\Models\PersonModel as PersonModel;
use App\Models\ScoreModel as ScoreModel;
use App\Entities\User as User;
use App\Entities\Person as Person;
use App\Entities\Score as Score;

use \App\Libraries\Mail as Mail;

class Users extends BaseController {

    protected $users;
    protected $user;
    protected $personas;
    protected $persona;

    protected $validation;

    public function __construct() {
        $this->users = new UserModel();
        $this->personas = new PersonModel();
        $this->validation =  \Config\Services::validation();
    }

    /**
     * list all users
     */
    public function index() {
        $this->response->setHeader('Content-Type', 'application/json');
        $response = array(
            'status' => 'ok',
            'message' => 'Data retrived',
            'data' => 'Users'
        );
        echo json_encode( $response );
    }

    /**
     * crea un usuario desde la app
     */
    public function crea() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( $this->validation->run( $data, 'signup' ) ) { // validar la integridad de los campos
            $this->user = new User( $data );
            $this->persona = new Person( $data );
            $searchName = $this->users->searchUserName($data['username']);
            if ( is_null($searchName) ) { // validar si el nombre de usuario no existe
                if ( $this->personas->save( $this->persona ) ) { // si se puede crear a la persona, continuar
                    $this->user->person = $this->personas->lastId();
                    $this->user = $this->users->registra( $this->user );
                    if ( !is_null($this->user) ) { // si se puede crear al usuario, continuar
                        $mail = new Mail();
                        $mail->registration( $this->persona->email, $this->persona->name, $this->user->validationToken );
                        $this->user->password = '';
                        $this->user->passwordConfirm = '';
                        $this->user->validationToken = '';
                        $response = array( 'status' => 'ok', 'message' => 'User created succesfuly.', 'data' => $this->user );
                    }
                    else {
                        $response = array( 'status' => 'error', 'message' => 'Cannot create new user.', 'data' => null );
                    }
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Cannot create new user.', 'data' => null );
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Username already exists', 'data' => null );
            }
        }
        else {
            foreach ( $this->validation->getErrors() as $error ) {
                $errors[] = $error;
            }
            $response = array( 'status' => 'error', 'message' => 'Cannot create new user.', 'data' => $errors );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * obtiene la información de un usuario con base en el ID proporcionado
     */
    public function ver($id) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
        $data = [ 'id'=>$id ];
        if ( $this->validation->run( $data, 'urlparameters') ) {
            $perfil = $this->users->getPerfil( $id );
            if ( $perfil ) {
                $response = array( 'status' => 'ok', 'message' => 'User\'s profile obtained.', 'data' => $perfil );
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'User not found.', 'data' => $perfil );
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Validation errors.', 'data' => $this->getValidationErrors($this->validation) );
        }

        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * verifica un toke de activacion
     */
    public function verify($token) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => null );
        $UserModel = new UserModel();

        $validationResult = $UserModel->validaToken( $token );
        if ( $validationResult === true ) {
            $response = array( 'status' => 'ok', 'message' => 'User has been validated.', 'data' => null );
        }
        else {
            $response = array( 'status' => 'error', 'message' => $validationResult, 'data' => null );
        }

        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * reenvia el correo de bienvenida
     */
    public function resend($userId) {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => $this->user );
        if ( is_numeric($userId) ) {
            $user = $this->users->find( $userId );
            if ( !is_null($user) ) {
                $persona = $this->personas->find( $user->person );
                if ( !is_null($persona) ) {
                    $user->active = '0';
                    $user->validationToken = $this->users->creaToken( $user->username );
                    $this->users->save( $user );
                    $mail = new Mail();
                    $mail->registration( $persona->email, $persona->name, $user->validationToken );
                    $response = array( 'status' => 'ok', 'message' => 'Token sent.', 'data' => $user );
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Cannot find person data.', 'data' => null );
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Username cannot be found.', 'data' => null );
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Cannot find user with information provided.', 'data' => null );
        }
        
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

    /**
     * recibe los parametros para dar una calificacion al usuario
     */
    public function califica() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started.', 'data' => $this->user );
        $data = $this->request->getJSON(true); 
        if ( $data ) {
            if ( array_key_exists('user', $data) && array_key_exists('score', $data) ) {
                $ScoreModel = new ScoreModel();
                $registra = $ScoreModel->registra( $data['user'], $data['score'] );
                if ( $registra === true ) {
                    $response = array( 'status' => 'ok', 'message' => 'Score registred successfully.', 'data' => null );    
                }
                else {
                    $response = array( 'status' => 'error', 'message' => $registra, 'data' => null );        
                }
            }
            else {
                $response = array( 'status' => 'error', 'message' => 'Not enough parameters.', 'data' => null );    
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Bad request.', 'data' => null );
        }
        
        $this->response->setHeader('Content-Type', 'application/json');
        echo $this->cleanResponse($response);
    }

}