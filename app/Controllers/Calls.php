<?php
namespace App\Controllers;

use App\Models\CallModel as CallModel;
use App\Models\TripModel as TripModel;
use App\Models\PlaceModel as PlaceModel;
use App\Models\UserModel as UserModel;

use App\Entities\Call as Call;
use App\Entities\Trip as Trip;
use App\Entities\Place as Place;

class Calls extends BaseController {

    protected $CallModel;
    protected $TripModel;
    protected $PlaceModel;

    protected $validation;

    public function __construct() {
        $this->CallModel = new CallModel();
        $this->TripModel = new TripModel();
        $this->PlaceModel = new PlaceModel();
        $this->UserModel = new UserModel();

        $this->validation =  \Config\Services::validation();
    }

    public function index() {
        
        $this->response->setHeader('Content-Type', 'application/json');
        $response = array(
            'status' => 'ok',
            'message' => 'Data retrived',
            'data' => null
        );
        echo json_encode( $response );

    }

    public function ver($id) {
        $calls = new CallModel();

        $id = intval($id);

        $this->response->setHeader('Content-Type', 'application/json');
        $registro = $this->CallModel->find($id);
        if ( $registro['totalRegistros'] === 1 ) {
            $response = array(
                'status' => 'ok',
                'message' => 'Data retrived',
                'data' => $registro
            );
            echo json_encode( $response );
        }
        else {
            $this->response->setStatusCode(404);
            $response = array(
                'status' => 'error',
                'message' => 'Not found',
                'data' => null
            );
            echo json_encode( $response );
        }
        
    }

    /**
     * crea el registro de una llamada, validando los lugares de origen , destino y creando el viaje
     */
    public function crea() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( $this->validation->run( $data, 'newcall' ) ) {
            $pickup = $this->PlaceModel->registra( $this->getPrefixedData( $data, 'pickup' ) );
            $destination = $this->PlaceModel->registra( $this->getPrefixedData( $data, 'dest' ) );
            if ( $data['client'] == '0' ) {
                $response = $this->CallModel->registra( $data, $pickup, $destination );
                if ( $response['status'] == 'ok' ) {
                    $dataResponse = $this->creaResponseNewCall($pickup, $destination, $response['data']['trip'], $response['data']['call']);
                    $response = array( 'status' => 'ok', 'message' => 'Trip requested successfully.',  'data' => $dataResponse );
                }
            }
            else {
                $client = $this->UserModel->find( $data['client'] );
                if ( !is_null($client) ) {
                    if ( $client->ontrip == '0' ) {
                        $response = $this->CallModel->registra( $data, $pickup, $destination );
                        if ( $response['status'] == 'ok') {
                            $pago = $this->TripModel->paga( $response['data']['trip'], $response['data']['call'] );
                            if ( $pago['status'] == 'ok' ) {
                                $client->ontrip = '1';
                                $this->UserModel->save( $client );
                                $dataResponse = $this->creaResponseNewCall($pickup, $destination, $response['data']['trip'], $response['data']['call']);
                                $response = array( 'status' => 'ok', 'message' => 'Trip requested successfully.',  'data' => $dataResponse );
                            }
                            else {
                                $this->TripModel->delete( $response['data']['trip']->id );
                                $this->CallModel->delete( $response['data']['call']->id );
                                $response = array( 'status' => 'error', 'message' => $pago['message'], 'data' => $pago['data'] );
                            }
                        }
                    }
                    else {
                        $response = array( 'status' => 'error', 'message' => 'Customer is currently in other trip.',  'data' => null );    
                    }
                }
                else {
                    $response = array( 'status' => 'error', 'message' => 'Customer not found.',  'data' => null );
                }
            }
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Please check error messages.', 'data' => $this->getValidationErrors($this->validation) );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $response );
    }

    public function estima() {
        $response = array( 'status' => 'error', 'message' => 'Proccess not started', 'data' => null );
        $data = $this->request->getJSON(true); 
        if ( !is_null($data) ) {
            $data['carType'] = '1';
        }
        if ( $this->validation->run( $data, 'newcall' ) ) {
            $response = array(
                'status' => 'ok',
                'message' => 'Estimates',
                'data' => $this->TripModel->estima( $data['pickupMapsCoord'], $data['destMapsCoord'] )
            );
        }
        else {
            $response = array( 'status' => 'error', 'message' => 'Please check error messages.', 'data' => $this->getValidationErrors($this->validation) );
        }
        $this->response->setHeader('Content-Type', 'application/json');
        echo json_encode( $response );
    }

    /**
     * crea el arreglo con los datos necesarios para enviar al socket con la informacion de 
     * la llamada creada. 
     * @param pickup el objeto con los datos de la recogida
     * @param destination el objeto con los datos del destino del viaje
     * @param trip el objeto con los datos del viaje
     * @param call el objeto con los datos de la solicitud
     * @return data el arreglo con los datos necesarios para renderizar la solicitud
     */
    protected function creaResponseNewCall( $pickup, $destination, $trip, $call ) {
        $data = array(
            'id' => $call->id,
            'client' => $call->client,
            'trip' => $trip->id,
            'daynumber' => $call->dayNumber,
            'line' => '0',
            'datecall' => $call->dateTime,
            'calltimestamp' => $call->timestamp,
            'phone' => $call->phone,
            'status' => $call->status,
            'dispatchdate' => $call->dispatchDate,
            'dispatchtype' => $call->dispatchType,
            'registertype' => $call->registerType,
            'amount' => $trip->amount,
            'eta' => $trip->eta,
            'distance' => $trip->distance,
            'username' => 'API',
            
            'pickupmapsid' => $pickup->mapsId,
            'pickupmapscoord' => $pickup->mapsCoord,
            'pickupname' => $pickup->mapsName,
            'pickupnumber' => $pickup->number,
            'pickupstreet' => $pickup->street,
            'pickupcity' => $pickup->city,
            'pickupstate' => $pickup->state,
            'pickupzip' => $pickup->zip,
            'pickupcountry' => $pickup->country,
            
            'destmapsid' => $destination->mapsId,
            'destmapscoord' => $destination->mapsCoord,
            'destname' => $destination->mapsName,
            'destnumber' => $destination->number,
            'deststreet' => $destination->street,
            'destcity' => $destination->city,
            'deststate' => $destination->state,
            'destzip' => $destination->zip,
            'destcountry' => $destination->country
        );

        return $data;
    }

}