<?php
namespace App\Libraries;
require_once ( APPPATH.'ThirdParty/stripe-php-7-42-0/init.php' );

class StripePcs { 

    protected $apikey;
    protected $stripe;

    protected $defaultMoneda = 'usd';

    public function __construct() {
        $this->apikey = getenv('stripe.apikey');
        \Stripe\Stripe::setApiKey( $this->apikey );
        $this->stripe = new \Stripe\StripeClient( $this->apikey );
    }

    /**
     * realiza un pago a traves de la api de stripe
     * @param cardData los datos de la tarjeta en arreglo ( number, expMonth, expYear, cvc )
     * @param chargeData los datos para hacer el cobro, en un arraglo ( monto(centavos), moneda, descripcion, email )
     * @return response arreglo con los datos para validar si se agrego correcto o no (status, message, data)
     */
    public function paga( $cardData, $chargeData ) {
        try {
            $token = $this->stripe->tokens->create([ 
                    'card' => [
                    'number'    => $cardData['cardNumber'],
                    'exp_month' => $cardData['expMonth'],
                    'exp_year'  => $cardData['expYear'],
                    'cvc'       => $cardData['cvc'],
                ],
            ]);
    
            $charge = $this->stripe->charges->create([
                'amount'        => $chargeData['monto'],
                'currency'      => $chargeData['moneda'],
                'source'        => $token->id,
                'description'   => $chargeData['descripcion'],
                'receipt_email' => 'mxmigue@gmail.com'
            ]);

            $response = array(
                'status' => 'ok',
                'message' => 'Payment succeded',
                'data' => $charge
            );
        } 
        catch(\Stripe\Exception\CardException $e) {
            // Since it's a decline, \Stripe\Exception\CardException will be caught
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        } catch (\Stripe\Exception\RateLimitException $e) {
            // Too many requests made to the API too quickly
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            // Invalid parameters were supplied to Stripe's API
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            // Network communication with Stripe failed
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        } catch (\Stripe\Exception\ApiErrorException $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $response = array(
                'status' => 'error',
                'message' => $e->getError()->message,
                'data' => array(
                    'statusCode' => $e->getHttpStatus(),
                    'type' => $e->getError()->type,
                    'code' => $e->getError()->code,
                    'param' => $e->getError()->param,
                )
            );
        }

        return $response;
    }

    /**
     * agrega una nueva tarjeta a un cliente determinado; si el cliente no existe o la tarjeta no pasa, la operacion no se completa
     * @param cardData in arreglo con los datos de la tarjeta ( cardNumber, expYear, expMonth, cvc )
     * @return result un arreglo con el resultado de la operacion ( status[ok|error], mensaje )
     */
    public function addCard( $cardData ) {
        $result = array( 'status'=>'error', 'message'=>'Operation not started.' );
        $pagoInicial = array(
            'monto' => '50',
            'moneda' => $this->defaultMoneda,
            'descripcion' => 'PrivateCarService New payment method'
        );
        $result = $this->paga( $cardData, $pagoInicial );

        return $result;
    }

    public function pagatest() {
        \Stripe\Stripe::setApiKey( $this->apikey );
        $stripe = new \Stripe\StripeClient( $this->apikey );
        $token = $stripe->tokens->create([
            'card' => [
                'number' => '4000004840008001',
                'exp_month' => 7,
                'exp_year' => 2021,
                'cvc' => '314',
            ],
        ]);

        $charge = $stripe->charges->create([
            'amount' => 2000,
            'currency' => 'usd',
            'source' => $token->id,
            'description' => 'My First Test Charge (created for API docs)',
        ]);
        
    }
}