<?php

namespace App\Entities;
use CodeIgniter\Entity;

class Trip extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'call' => null,
        'driver' => null,
        'pickup' => null,
        'destination' => null,
        'status' => null,
        'amount' => null,
        'eta' => null,
        'startTime' => null,
        'endTime' => null,
        'distance' => null,

        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'trip_id',
        'call' => 'trip_call',
        'driver' => 'trip_driver',
        'pickup' => 'trip_pickup',
        'destination' => 'trip_destination',
        'status' => 'trip_status',
        'amount' => 'trip_amount',
        'distance' => 'trip_distance',
        'eta' => 'trip_eta',
        'startTime' => 'trip_start_time',
        'endTime' => 'trip_end_time',
        

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}