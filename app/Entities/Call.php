<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Call extends Entity
{
    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'operator' => null,
        'carType' => null,
        'client' => null,
        'dayNumber' => null,
        'line' => null,
        'dateTime' => null,
        'timestamp' => null,
        'phone' => null,
        'status' => null,
        'dispatchDateTime' => null,
        'dispatchTimestamp' => null,
        'dispatchType' => null,
        'registerType' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'call_id',
        'operator' => 'call_operator',
        'carType' => 'call_car_type',
        'client' => 'call_client',
        'dayNumber' => 'call_day_number',
        'line' => 'call_line',
        'dateTime' => 'call_date_time',
        'timestamp' => 'call_timestamp',
        'phone' => 'call_phone',
        'status' => 'call_status',
        'dispatchDateTime' => 'call_dispatch_date_time',
        'dispatchTimestamp' => 'call_dispatch_timestamp',
        'dispatchType' => 'call_dispatch_type',
        'registerType' => 'call_register_type'
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];
}