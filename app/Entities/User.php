<?php

namespace App\Entities;

use CodeIgniter\Entity;

class User extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'role' => null,
        'person' => null,
        'username' => null,
        'password' => null,
        'client' => null,
        'status' => null,
        'online' => null,
        'ontrip' => null,
        'validationToken' => null,
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'user_id',
        'role' => 'user_role',
        'person' => 'user_person',
        'username' => 'user_username',
        'password' => 'user_password',
        'client' => 'user_client',
        'status' => 'user_status',
        'online' => 'user_online',
        'ontrip' => 'user_ontrip',
        'validationToken' => 'user_validation_token',
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}