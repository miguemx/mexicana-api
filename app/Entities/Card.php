<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Card extends Entity {
    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'user' => null,
        'cardNumber' => null,
        'expMonth' => null,
        'expYear' => null,
        'cvc' => null,
        'name' => null,
        'default' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'card_id',
        'user' => 'card_user',
        'cardNumber' => 'card_number',
        'expMonth' => 'card_exp_month',
        'expYear' => 'card_exp_year',
        'cvc' => 'card_cvc',
        'name' => 'card_name',
        'default' => 'card_default',
        
        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];
}