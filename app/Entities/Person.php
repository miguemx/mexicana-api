<?php
namespace App\Entities;

use CodeIgniter\Entity;

class Person extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'name' => null,
        'address' => null,
        'phone' => null,
        'cellphone' => null,
        'email' => null,
        'city' => null,
        'state' => null,
        'zip' => null,
        'birth' => null,
        'ssNumber' => null,
        'usCitizen' => null,
        
        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'person_id',
        'name' => 'person_name',
        'address' => 'person_address',
        'phone' => 'person_phone',
        'cellphone' => 'person_cellphone',
        'email' => 'person_email',
        'city' => 'person_city',
        'state' => 'person_state',
        'zip' => 'person_zip',
        'birth' => 'person_birth',
        'ssNumber' => 'person_ss_num',
        'usCitizen' => 'person_us_citizen',

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}