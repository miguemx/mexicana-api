<?php
namespace App\Entities;
use CodeIgniter\Entity;

class Place extends Entity {

    // poner los atributos que deseamos sean visibles en toda la aplicacion
    protected $attributes = [
        'id' => null,
        'mapsId' => null,
        'mapsCoord' => null,
        'mapsName' => null,
        'number' => null,
        'street' => null,
        'city' => null,
        'state' => null,
        'zip' => null,
        'country' => null,

        'created_at' => null,
        'updated_at' => null,
        'deleted_at' => null,
    ];

    // al hacer el mapeo, se pone como llave el atributo y como valor el campo en la base de datos
    // y no como dice la documentacion de CI que lo menciona alreves.
    protected $datamap = [
        'id' => 'place_id',
        'mapsId' => 'place_maps_id',
        'mapsCoord' => 'place_maps_coord',
        'mapsName' => 'place_maps_name',
        'number' => 'place_number',
        'street' => 'place_street',
        'city' => 'place_city',
        'state' => 'place_state',
        'zip' => 'place_zip',
        'country' => 'place_country',

        'created_at' => 'created_at',
        'updated_at' => 'updated_at',
        'deleted_at' => 'deleted_at',
    ];

    protected $casts = [
        'options' => 'array',
                'options_object' => 'json',
                'options_array' => 'json-array'
    ];

}