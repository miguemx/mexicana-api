<?php namespace Config;

class Validation
{
	//--------------------------------------------------------------------
	// Setup
	//--------------------------------------------------------------------

	/**
	 * Stores the classes that contain the
	 * rules that are available.
	 *
	 * @var array
	 */
	public $ruleSets = [
		\CodeIgniter\Validation\Rules::class,
		\CodeIgniter\Validation\FormatRules::class,
		\CodeIgniter\Validation\FileRules::class,
		\CodeIgniter\Validation\CreditCardRules::class,
	];

	/**
	 * Specifies the views that are used to display the
	 * errors.
	 *
	 * @var array
	 */
	public $templates = [
		'list'   => 'CodeIgniter\Validation\Views\list',
		'single' => 'CodeIgniter\Validation\Views\single',
	];

	//--------------------------------------------------------------------
	// Rules
	//--------------------------------------------------------------------

	// ------------------------------------ for URL parameters
	public $urlparameters = [
        'id' => 'numeric',
	];
	public $urlparameters_errors = [
		'id' => [ 
			'numeric' => 'Please enter a valid parameter.'
		],
	];

	// -------------------- for signup
	public $signup = [
        'username'        => 'required',
        'password'        => 'required',
		'passwordConfirm' => 'required|matches[password]',
		'name'         => 'required|regex_match[/^[A-Za-z\sñÑáéíóúÁÉÍÓÚ]+$/]',
		'email'           => 'required|valid_email',
		'cellphone'       => 'required|numeric'
	];
	public $signup_errors = [
		'username' => [ 'required' => 'You must choose an username.', ],
		'password' => [ 'required' => 'You must choose a password.', ],
		'passwordConfirm' => [ 
			'required' => 'Please confirm your password.',
			'matches' => 'Passwords do not match.',
		],
        'email' => [
			'required' => 'Please provide your email.',
            'valid_email' => 'Your email does not appear to be valid.'
		],
		'name' => [
			'required' => 'Please provide your name.',
            'regex_match' => 'Your name does not appear to be valid.'
        ]
		
	];
	
	// ------------------------------------ for login
	public $login = [
        'username'        => 'required',
        'password'        => 'required',
	];
	public $login_errors = [
		'username' => [ 'required' => 'Please provide your username.', ],
		'password' => [ 'required' => 'Please provide your password.', ],
	];
	
	// ------------------------------------ for call registering
	public $newcall = [
		'carType'         => 'required',
		'client'          => 'required',
		
		'pickupMapsCoord' => 'required|regex_match[/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/]',

		'destMapsCoord'   => 'required|regex_match[/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/]',
		
	];
	public $newcall_errors = [
		'carType' => [ 'required' => 'Please select a car type.', ],
		'client'  => [ 'required' => 'You must specify the client. If you are on your app, please logout and login again.', ],
		
		'pickupMapsCoord' => [ 
			'required' => 'Please provide your pickup location.', 
			'regex_match' => 'Your location does not appear to be valid.'
		],

		'destMapsCoord' => [ 
			'required' => 'Please provide your destination location.', 
			'regex_match' => 'Your location does not appear to be valid.'
		],
	];
	
	// ------------------------------------ for credit card details
	public $card = [
        'cardNumber' => 'required|regex_match[/^[0-9\s]+$/]|max_length[20]',
		'expYear'    => 'required|greater_than_equal_to[2020]|less_than_equal_to[3000]',
		'expMonth'   => 'required|greater_than_equal_to[1]|less_than_equal_to[12]',
		'cvc'        => 'required|integer|max_length[4]',
	];
	public $card_errors = [
		'cardNumber' => [ 
			'required' => 'No card number given.', 
			'regex_match' => 'Please enter a valid card number. Only numbers and spaces allowed'
		],
		'expYear' => [ 
			'required' => 'Please enter expiration year.', 
			'greater_than_equal_to' => 'Expiration year does not appear to be valid.',
			'less_than_equal_to' => 'Expiration year does not appear to be valid.'
		],
		'expMonth' => [ 
			'required' => 'Please enter expiration month.', 
			'greater_than_equal_to' => 'Expiration month must be a number between 1 and 12.',
			'less_than_equal_to' => 'Expiration month must be a number between 1 and 12.'
		],
		'cvc' => [ 
			'required' => 'Please enter your card\'s CVC.', 
			'integer' => 'CVC must be a number.',
			'max_length' => 'CVC must be a number with length less than 4.'
		],
		
	];



}
